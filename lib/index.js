"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.Error = exports.Response = undefined;
exports.default = init;

var _response = require("./response");

var _response2 = _interopRequireDefault(_response);

var _error = require("./error");

var _error2 = _interopRequireDefault(_error);

var _server = require("./server.active");

var _server2 = _interopRequireDefault(_server);

var _server3 = require("./server.static");

var _server4 = _interopRequireDefault(_server3);

var _map = require("./map");

var _map2 = _interopRequireDefault(_map);

var _link = require("./link");

var _link2 = _interopRequireDefault(_link);

var _router = require("./router");

var _router2 = _interopRequireDefault(_router);

var _url = require("./url");

var _url2 = _interopRequireDefault(_url);

var _info = require("./info");

var _info2 = _interopRequireDefault(_info);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Response = _response2.default;
exports.Error = _error2.default;
/*
export {
	create, set, remove, get,
	setLink, removeLink,
	setMap, removeMap, findMap,
	getUrl,
} from "./base";

export const server = {
	 active: activeServer,
	 static: staticServer,
}
*/

function getUserStatus(cfg) {
	let tokenName = "user-token";
	let login = x => false;
	let exit = x => false;
	let checkCookie = x => false;

	const config = {
		get tokenName() {
			return tokenName;
		},
		get login() {
			return login;
		},
		get exit() {
			return exit;
		},
		get checkCookie() {
			return checkCookie;
		},

		set tokenName(v) {
			if (typeof v === "string") {
				tokenName = v;
			}
		},
		set login(v) {
			if (typeof v === "function") {
				login = v;
			} else if (v === null) {
				login = x => false;
			}
		},
		set exit(v) {
			if (typeof v === "function") {
				exit = v;
			} else if (v === null) {
				exit = x => false;
			}
		},
		set checkCookie(v) {
			if (typeof v === "function") {
				checkCookie = v;
			} else if (v === null) {
				checkCookie = x => false;
			}
		}
	};
	cfg = cfg || {};
	config.tokenName = cfg.tokenName;
	config.login = cfg.login;
	config.exit = cfg.exit;
	config.checkCookie = cfg.checkCookie;
	return config;
}
function getUserSystem(cfg) {
	let getUserInfo = x => null;
	let getUserRole = x => [];
	let getUserPower = x => [];

	const config = {
		get getUserInfo() {
			return getUserInfo;
		},
		get getUserRole() {
			return getUserRole;
		},
		get getUserPower() {
			return getUserPower;
		},

		set getUserInfo(v) {
			if (typeof v === "function") {
				getUserInfo = v;
			} else if (v === null) {
				getUserInfo = x => null;
			}
		},
		set getUserRole(v) {
			if (typeof v === "function") {
				getUserRole = v;
			} else if (v === null) {
				getUserRole = x => [];
			}
		},
		set getUserPower(v) {
			if (typeof v === "function") {
				getUserPower = v;
			} else if (v === null) {
				getUserPower = x => [];
			}
		}
	};
	cfg = cfg || {};
	config.getUserInfo = cfg.getUserInfo;
	config.getUserRole = cfg.getUserRole;
	config.getUserPower = cfg.getUserPower;
	return config;
}

function getConfig(cfg) {
	let isDebug = false;
	let port = 0;
	let protocol = "";
	let cbName = "";
	let cookie = {};
	let getBasePath = function () {};
	let userStatus = {};
	let userSystem = {};
	const config = {
		get isDebug() {
			return isDebug;
		},
		get port() {
			return port;
		},
		get protocol() {
			return protocol;
		},
		get cbName() {
			return cbName;
		},
		get cookie() {
			return cookie;
		},
		get getBasePath() {
			return getBasePath;
		},
		get userStatus() {
			return userStatus;
		},
		get userSystem() {
			return userSystem;
		},

		set isDebug(v) {
			isDebug = Boolean(v);
		},
		set port(v) {
			if (v === 0 || (v = Number(v))) {
				port = v;
			}
		},
		set protocol(v) {
			if (typeof v === "string") {
				protocol = v;
			}
		},
		set cbName(v) {
			if (typeof v === "string") {
				cbName = v;
			}
		},
		set cookie(v) {
			if (typeof v === "object") {
				cookie = v;
			}
		},
		set getBasePath(v) {
			if (typeof v === "function") {
				getBasePath = v;
			}
		},
		set userStatus(v) {
			if (typeof v === "object") {
				userStatus = getUserStatus(v);
			}
		},
		set userSystem(v) {
			if (typeof v === "object") {
				userSystem = getUserSystem(v);
			}
		}
	};
	cfg = cfg || {};
	config.isDebug = cfg.isDebug;
	config.port = cfg.port;
	config.protocol = cfg.protocol;
	config.cbName = cfg.cbName;
	config.cookie = cfg.cookie;
	config.getBasePath = cfg.getBasePath;
	userStatus = getUserStatus(cfg.userStatus);
	userSystem = getUserSystem(cfg.userSystem);
	return config;
}
function init(cfg) {
	const config = getConfig(cfg);
	const map = (0, _map2.default)();
	const link = (0, _link2.default)();
	const router = (0, _router2.default)();

	const url = (0, _url2.default)(map.find, config);
	const info = (0, _info2.default)(config, url.get);

	return {
		Response: _response2.default,
		Error: _error2.default,

		setMap: map.set,
		removeMap: map.remove,
		findMap: map.find,
		mapList: map.list,
		updateMap: map.update,

		setLink: link.set,
		removeLink: link.remove,
		getLink: link.get,

		create: router.create,
		set: router.set,
		remove: router.remove,
		get: router.get,

		getUrl: url.get,

		server: {
			static: (0, _server4.default)(config, map.list, link.get, router.get, info),
			active: (0, _server2.default)(config, map.list, link.get, router.get, info)
		},
		get config() {
			return config;
		}
	};
}