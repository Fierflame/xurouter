"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = create;


/**
 * 将传统处理函数转为Restful API 函数
 * @param  {Function}	func	原始函数
 * @param  {Object}		argv	restful 路径参数
 * @param  {String}		method	HTTP 方法
 * @return {Function}			Restful API 函数
 */
function createRestful(func, argv, method) {
	return (data, info, ...p) => {
		const query = info && info.query || {};
		for (let k in query) {
			if (/^[^$_]/.test(k)) {
				argv["$" + k] = query[k];
			}
		}
		argv.$ = method || info && info.method || "";
		return func(argv, info, ...p);
	};
}

/**
 * 根据信息获取函数
 * @param  {Function}	func	原始函数
 * @param  {String}		method	HTTP 方法
 * @param  {String[][]}	opt		restful 路径参数键值对组
 * @param  {Object}		restful	是否强制当restful API函数处理
 * @return {Function}			Restful API 函数
 */
function getFunc(func, method, opt, restful) {
	//如果是对象，将视其$为函数
	if (func && typeof func === "object") {
		func = func.$;
		if (func && typeof func === "object" && method in func) {
			func = func[method];
		}
	}
	//如果不是函数，则直接返回
	if (typeof func !== "function") {
		return;
	}
	//若不按restful API处理则直接返回
	if (!(opt.length || restful)) {
		return func;
	}
	//路径参数处理
	let argv = {};
	for (let [k, v] of opt) {
		argv[k] = v;
	}
	//创建restful函数
	return createRestful(func, argv, method);
}

const keyTestRegExp = /^([a-zA-Z_][a-zA-Z0-9_]+)$/;

function* routerBase(func, path, method, restful = false, index = 0, opt = []) {
	//函数组无效，则直接返回
	if (!func) {
		return;
	}
	let key = path[index++];
	//路径已经取完，则进行返回处理
	if (!key) {
		func = getFunc(func, method, opt, restful);
		if (func) {
			yield func;
		}
		return;
	}
	//不以以美元符号($)、下划线(_)、冒号(:)开头的路径，尝试当做普通路径处理
	if (key in func && !(key[0] === "$" || key[0] === "_" || key[0] === ":")) {
		yield* routerBase(func[key], path, method, restful, index, opt);
	}
	//按照restful API路径处理
	for (let k in func) {
		if (k[0] !== "$" && k[0] !== ":") {
			continue;
		}
		const vk = k.substr(1);
		if (!keyTestRegExp.test(vk)) {
			continue;
		}
		opt.push([vk, key]);
		yield* routerBase(func[k], path, method, restful, index, opt);
		opt.pop();
	}
}

/**
 * 默认路由
 * @this {Object}
 * @param {String[]}	path	路径
 * @param {String}		method	方法
 * @return {Function}			函数
 */
function* router(path, method) {
	yield* routerBase(this, path, method && String(method).toLowerCase());
}

/**
 * 创建函数路由
 * @param		{Object} 	f	函数集合
 * @param		{Function} 	r	自定义处理器
 */
function create(f, r) {
	f = router.bind(f);
	return r ? (p, ...m) => r(p, f, ...m) : f;
}