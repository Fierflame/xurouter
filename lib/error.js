"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
//错误列表

const errorList = {
	":system:": "系统错误！",
	":user:nologin": "暂未登录，请先登录",
	":power:nopower": "没有操作权限",
	":domain:origin": "无法判断来源",
	":domain:": "非法访问"
};
class ActiveError extends Error {
	constructor(id, msg) {
		id = String(id);
		if (msg && typeof msg === "string") {
			msg = msg;
		} else if (id in errorList) {
			msg = errorList[id];
		} else {
			msg = "";
		}
		super(msg);
		this.error = id;
		this.msg = msg;
	}
	toJSON() {
		return { error: this.error, msg: this.msg };
	}
	toString() {
		return this.msg + "(" + this.error + ")";
	}
	static set(id, title) {
		errorList[id] = title;
	}
}
exports.default = ActiveError;