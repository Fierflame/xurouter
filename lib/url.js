"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.get = get;
exports.default = init;

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _map = require("./map");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * 根据功能路径获取Url
 * @param  {String} pathId 功能路径
 * @return {String}        实际Url
 */
function get(pathId) {
	let [id, ep = ""] = String(pathId).split(":");
	id = id.split("@");
	try {
		const [basePath, domain] = (this && this.findMap || _map.find)(id[0], id[1]);
		const port = this && this.port || "";
		const protocol = this && this.protocol || "";
		return (domain && protocol + "//" + domain + (port && ":" + port)) + _path2.default.join("/", basePath, _path2.default.join("/", ep));
	} catch (e) {
		return "";
	}
}
function init(findMap, config) {
	return {
		get: get.bind({
			findMap,
			get port() {
				return config.port;
			},
			get protocol() {
				return config.protocol;
			}
		})
	};
}