"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.server = server;
exports.default = init;

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _response = require("./response");

var _response2 = _interopRequireDefault(_response);

var _util = require("./util.path-match");

var _util2 = _interopRequireDefault(_util);

var _map = require("./map");

var _link = require("./link");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//基本文档路径和应用路径
let htdocsPath, appPath;
const stat = path => new Promise((a, b) => _fs2.default.stat(path, (e, d) => e ? b(e) : a(d)));
/**
 * 获取真实路径
 * @param  {String} p 转化后路径
 * @return {Array}   真实路径信息
 */
async function getRealPath(realPath, rpath, vpath) {
	//如果为目录，则返回index.html
	if (realPath[realPath.length - 1] === "/") {
		return new _response2.default({ file: realPath + "index.html" });
	}
	//如果文件存在,且为文件，则返回
	let isDir;
	try {
		let info = await stat(realPath);
		if (info.isFile()) {
			return new _response2.default({ file: realPath });
		}
		isDir = info.isDirectory();
	} catch (e) {}
	//如果文件.html存在，且为文件，则返回
	try {
		if ((await stat(realPath + ".html")).isFile()) {
			return new _response2.default({ file: realPath + ".html" });
		}
	} catch (e) {}
	//如果文件夹存在，则302
	if (isDir) {
		return new _response2.default({ transfer: `${vpath}${rpath}/` });
	}
}
function getBasePath(appId) {
	return appId ? `${appPath}/${appId}/htdocs/` : `${htdocsPath}/`;
}
/**
 * 静态页面
 * @param {String}	staticPath	路径
 */
async function server(staticPath) {
	//路径应为文件夹的数组
	let map = this && this.mapList || _map.list;
	let link = this && this.getLink || _link.get;
	let basePath = this && this.basePath || getBasePath;

	staticPath = Array.isArray(staticPath) ? staticPath.map(String) : typeof staticPath === "string" ? staticPath.split(/[\\\/]+/) : [];

	//循环找到第一个路径环境
	for (let [vpath, path, appId] of (0, _util2.default)(staticPath, map, link)) {
		if (typeof path === "string") {
			let rpath = staticPath.slice(vpath.length);
			rpath = rpath.length ? "/" + rpath.join("/") : "";
			//获取实际路径
			const realPath = basePath(appId) + path + rpath;
			let res;
			if (res = await getRealPath(realPath, rpath, vpath.length > 1 ? "/" + vpath.slice(1).join("/") : "")) {
				return res;
			}
		}
	}
}

function init(config, mapList, getLink, getRouter) {
	return server.bind({
		basePath: appId => config.getBasePath(appId),
		mapList, getLink, getRouter
	});
}