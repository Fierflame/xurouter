"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.create = undefined;
exports.set = set;
exports.remove = remove;
exports.get = get;
exports.default = init;

var _router = require("./router.create");

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.create = _router2.default;

const list = {};
/**
 * 设置路由
 * @param		{String} 	path	路径
 * @param		{String} 	[appId]	应用Id
 * @param		{Object} 	f	    函数集合
 * @param		{Function} 	r	    自定义处理器
 */
function set(...p) {
	const routers = this || list;
	if (typeof p[1] !== "string") {
		p.splice(1, 0, "");
	}
	const [path, appId, f, r] = p;
	routers[appId ? path + "@" + appId : path] = (0, _router2.default)(f, r);
}

/**
 * 移除路由
 * @param		{String} 	path	路径
 * @param		{String} 	appId	应用Id
 */
function remove(path, appId) {
	const routers = this || list;
	delete routers[appId ? path + "@" + appId : path];
}

/**
 * 获取路由
 * @param		{String} 	path	路径
 * @param		{String} 	appId	应用Id
 */
function get(path, appId) {
	const routers = this || list;
	return routers[appId ? path + "@" + appId : path];
}

function init(routers = {}) {
	return {
		create: _router2.default,
		set: set.bind(routers),
		remove: remove.bind(routers),
		get: get.bind(routers)
	};
}