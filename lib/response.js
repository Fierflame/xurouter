"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _stream = require("stream");

var _stream2 = _interopRequireDefault(_stream);

var _error = require("./error");

var _error2 = _interopRequireDefault(_error);

var _util = require("./util.mime");

var _util2 = _interopRequireDefault(_util);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * 处理相应类
 */
class Response {
	Error(...p) {
		return new _error2.default(...p);
	}
	constructor(arg) {
		this.Error = function (...p) {
			return new _error2.default(...p);
		};
		this._valid = true;
		if (typeof arg === "string") {
			this._callback = arg;
		} else if (arg) {
			const { callback, error, statusCode, value, stream, mime, download, transfer, file } = arg;
			if (typeof callback === "string") {
				this._callback = callback;
			}
			this.error = error;
			this.statusCode = statusCode;
			this.mime = mime;
			this.download = download;
			this.transfer = transfer;
			this.file = file;
			this.value = value || stream;
		}
	}
	get valid() {
		return this._valid;
	}
	get error() {
		return this._error;
	}
	get statusCode() {
		if (this._transfer) {
			return 302;
		}
		let error = this._error;
		if (error && !error.error) {
			return 500;
		}
		return this._statusCode;
	}
	get value() {
		let error = this._error,
		    value = this._value,
		    cb = this._callback;
		if (!error) {
			if (value instanceof _stream2.default.Readable) {
				return;
			}
		}
		if (error) {
			if (!error.error) {
				console.error(error);
				value = new _error2.default(":system:");
			} else {
				value = error;
			}
		}
		if (!value) {
			return;
		}
		if (value instanceof Buffer) {
			return value;
		}
		try {
			value = JSON.stringify(value);
		} catch (e) {
			value = `{error:":system:", msg:"系统错误!"}`;
		}
		return cb ? `${cb}(${value})` : value;
	}
	get mime() {
		if (this._error) {
			if (this._callback) {
				return "application/javascript";
			}
			return "application/json";
		}
		if (this._transfer) {
			return;
		}
		if (this._mime && this._mime[0] !== ".") {
			return this._mime;
		}
		if (this._mime || this._file) {
			return (0, _util2.default)(this._mime) || (0, _util2.default)(_path2.default.extname(this._file)) || "application/octet-stream";
		}
		if (this._value instanceof _stream2.default.Readable) {
			return;
		}
		if (this._value instanceof Buffer) {
			return;
		}
		if (this._callback) {
			return "application/javascript";
		}
		return "application/json";
	}
	get download() {
		if (this._error) {
			return;
		}
		if (this._transfer) {
			return;
		}
		return this._download;
	}
	get stream() {
		if (this._error) {
			return;
		}
		let value = this._value;
		if (value instanceof _stream2.default.Readable) {
			return value;
		}
	}
	get transfer() {
		if (this._error) {
			return;
		}
		return this._transfer;
	}
	get file() {
		if (this._error) {
			return;
		}
		return this._file;
	}

	set valid(v) {
		return this._valid = Boolean(v);
	}
	set error(v) {
		if (typeof v === "string") {
			v = new _error2.default(v);
		} else if (v && Array.isArray(v) && v.length === 2 && typeof v[0] === "string" && typeof v[1] === "string") {
			v = new _error2.default(...v);
		}
		this._error = v;
	}
	set statusCode(v) {
		if (typeof v === "number" && v >= 200 && v < 1000) {
			this._statusCode = parseInt(v);
		}
	}
	set value(v) {
		if (!v) {
			return;
		}
		this._file = undefined;
		this._transfer = undefined;
		this._value = v;
	}

	set mime(v) {
		if (!(v && typeof v === "string")) {
			return;
		}
		this._mime = v;
	}
	set download(v) {
		if (!(v && typeof v === "string")) {
			return;
		}
		this._download = v;
	}

	set stream(v) {
		this.value = v;
	}

	set transfer(v) {
		if (!(v && typeof v === "string")) {
			return;
		}
		this._file = undefined;
		this._value = undefined;
		this._transfer = v;
	}
	set file(v) {
		if (!(v && typeof v === "string")) {
			return;
		}
		this._transfer = undefined;
		this._value = undefined;
		this._file = v;
	}
}
exports.default = Response;
Response.Error = function (...p) {
	return new _error2.default(...p);
};