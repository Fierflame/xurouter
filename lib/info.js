"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.info = info;
exports.default = init;

var _error = require("./error");

var _error2 = _interopRequireDefault(_error);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * 封装用户信息
 * @param  {Function} options.getCookie   获取Cookie的基本函数
 * @param  {Function} options.setCookie   设置Cookie的基本函数
 * @param  {Function} options.clearCookie 清楚Cookie的基本函数
 * @param  {Function} options.getHeader   获取Header的基本函数
 * @param  {Function} options.setHeader   设置Header的基本函数
 * @return {Object}                       用户信息
 */
async function info({
	files, method, path,
	getCookie, setCookie, clearCookie,
	getHeader, setHeader
}, query, body) {
	//用到的私有成员
	let {
		getUrl,
		isDebug, cookieConfig,
		login, exit, checkCookie, tokenName,
		getUserInfo, getUserRole, getUserPower
	} = this || {};
	//文件列表
	files = files || [];
	//当前的Cookie
	let cookie = getCookie(tokenName);
	//用户ID, 用户权限, 用户角色, 用户信息
	let userId,
	    power = null,
	    role = null,
	    info;
	let origin = getHeader("origin"),
	    referer = getHeader("referer");
	//User-Agent, Authorization
	let host = getHeader("host") || "";
	let port = 0;
	if (origin) {
		origin = origin.replace(/:\d+$/g, "").replace(/^(?:[a-zA-Z0-9\-]+\:\/\/)/g, "");
	}
	if (host) {
		host = host.replace(/:(\d+)$/, (x, p) => (port = Number(p), ""));
	}
	//基础配置
	let config = {};

	let userInfo = {
		get isDebug() {
			return isDebug;
		},
		/** @type {String} 获取请求方法 */
		get method() {
			return method;
		},
		/** @type {String} 获取请求方法 */
		get path() {
			return path;
		},
		/** @type {String} 获取主机名 */
		get host() {
			return host;
		},
		/** @type {String} 获取端口 */
		get port() {
			return port || "";
		},
		/** @type {String} 获取请求来源域名 */
		get origin() {
			return origin;
		},
		/** @type {String} 获取请求来源路径 */
		get referer() {
			return referer;
		},
		/**
   * 获取Cookie
   * @param  {String} cname Cookie名称
   * @return {String}       Cookie值
   */
		getCookie(cname) {
			if (cname === tokenName) {
				return cookie;
			}
			if (cname && typeof cname === "string") {
				return getCookie(cname);
			}
			if (!cookie) {
				return '';
			}
			return tokenName + "=" + cookie;
		},
		/**
   * 设置Cookie
   * @param {String} cname  Cookie名称
   * @param {String} cookie Cookie值
   * @param {Object} opt    Cookie选项
   */
		setCookie(cname, cookie, opt) {
			if (cname === tokenName) {
				return false;
			}
			return setCookie(cname, cookie, opt || cookieConfig);
		},
		async clearCookie(cname, opt) {
			if (typeof cname !== "string") {
				opt = cname;
				cname = "";
			}
			if (cname === tokenName) {
				await userInfo.exit();
			}
			opt = opt || cookieConfig;
			if (cname) {
				return clearCookie(cname, opt);
			} else {
				return clearCookie(opt);
			}
		},
		/**
   * 获取模块的Url
   * @param  {String} id 模块Id
   * @return {String}    模块Id
   */
		getUrl,
		/** 获取文件列表 */
		get files() {
			return files;
		},
		/** 获取查询字段 */
		get query() {
			return query;
		},
		/** 获取请求体 */
		get body() {
			return body;
		},
		/**
   * 判断是否为授权域名
   * @param  {Number} level 限制级别
   */
		async verifyDomain(level = 0) {
			if (!origin) {
				return false;
			}
			if (level >= 0) {
				//jsonp域名

			}
			if (level >= 1) {
				//AJAX域名
			}
			if (level >= 2) {
				//子域名
				let k = origin.indexOf("." + host);
				if (k !== -1 && k + 1 + host.length === origin.length) {
					return true;
				}
			}
			if (level >= 3) {
				//当前域名
				if (origin === host) {
					return true;
				}
			}
			return false;
		},
		/**
   * 检查域名
   * @param  {Number} level 限制级别
   */
		async checkDomain(level = 0) {
			if (!origin) {
				throw new _error2.default(":domain:origin");
			}
			if (userInfo.verifyDomain(level)) {
				return true;
			}
			throw new _error2.default(":domain:");
		},
		/** @type {String | null} 获取用户ID */
		get userId() {
			return userId;
		},
		/**
   * 设置用户登录
   * @param  {String} uid 用户ID
   */
		async login(uid) {
			let ret;
			try {
				ret = await login(uid, config, userInfo);
			} catch (e) {
				return false;
			}
			if (!(ret && typeof ret === "string")) {
				return;
			}
			userId = uid;
			power = null;
			role = null;
			info = null;
			cookie = ret;
			await setCookie(tokenName, cookie, cookieConfig);
			return true;
		},
		/**
   * 用户退出
   */
		async exit() {
			try {
				exit(config, userInfo);
			} catch (e) {}
			userId = null;
			power = null;
			role = null;
			info = null;
			cookie = '';
			await clearCookie(tokenName, cookieConfig);
		},
		/**
   * 检查用户是否登录
   * 如果用户没有登录，则报错
   * @throws {SystemError}	If	用户没有登录
   */
		async checkLogin() {
			//登录判断
			if (!userId) {
				throw new _error2.default(":user:nologin");
			}
		},
		/**
   * 获取用户信息
   */
		async getInfo() {
			if (!userId) {
				return null;
			}
			try {
				return await getUserInfo(userId);
			} catch (e) {
				return null;
			}
		},
		/**
   * 获取用户角色
   */
		async getRole() {
			if (!userId) {
				return [];
			}
			try {
				return await getUserRole(userId);
			} catch (e) {
				return [];
			}
		},
		/**
   * 获取用户权限
   */
		async getPower() {
			if (!userId) {
				return [];
			}
			try {
				return await getUserPower(userId);
			} catch (e) {
				return [];
			}
		},
		/**
   * 判断用户是否有权限
   * 特别的，如果用户有超级权限，则视为用户有任意权限
   * @param  {...String} powers 要判断的权限
   * @return {Boolean}          用户是否有列出的权限之一
   */
		async hasPower(...powers) {
			if (!userId) {
				return false;
			}
			let power;
			try {
				power = await getUserPower(userId);
			} catch (e) {
				return false;
			}
			if ("::" in power) {
				return true;
			}
			powers = powers.filter(x => typeof x === "string");
			for (let i = 0, l = powers.length; i < l; i++) {
				let p = powers[i];
				if (p in power) {
					return true;
				}
				let k = p.indexOf("$");
				if (k === -1) {
					continue;
				}
				let v = p.substr(k + 1);
				p = p.substr(0, k);
				if (!(p in power)) {
					continue;
				}
				p = power[p];
				if (Array.isArray(p) && p.indexOf(v)) {
					return true;
				}
			}
			return false;
		},
		/**
   * 检查用户权限
   * 如果用户没有登录或者列出的权限用户均没有，则报错
   * 特别的，如果用户有超级权限，则视为用户有任意权限
   * @param  {...String} powers 检查的权限
   * @throws {SystemError}	If	用户没有登录或者列出的权限用户均没有
   */
		async checkPower(...powers) {
			//登录判断
			if (!userId) {
				throw new _error2.default(":user:nologin");
			}
			if (await userInfo.hasPower(...powers)) {
				return true;
			}
			throw new _error2.default(":power:nopower");
		},
		/**
   * 获取用户ID
   * @return {String} 用户Id
   */
		async getId() {
			return userId;
		}
	};

	try {
		let uid = await checkCookie(cookie, config, userInfo);
		if (uid && typeof uid === "string") {
			userId = uid;
		} else if (Array.isArray(uid) && (uid = uid.filter(x => x && typeof x === "string")).length) {
			userId = uid[0];
			if (uid[1]) {
				cookie = uid[1];
			}
		} else {
			cookie = "";
		}
	} catch (e) {
		cookie = "";
	}
	return userInfo;
}

function init(config, getUrl) {
	return info.bind({
		getUrl,
		get isDebug() {
			return config.isDebug;
		},
		get cookieConfig() {
			return config.cookie;
		},
		get tokenName() {
			return config.userStatus.tokenName || "user-token";
		},
		login: (...p) => config.userStatus.login(...p),
		exit: (...p) => config.userStatus.exit(...p),
		checkCookie: (...p) => config.userStatus.checkCookie(...p),

		getUserInfo: id => config.userSystem.getUserInfo(id),
		getUserRole: id => config.userSystem.getUserRole(id),
		getUserPower: id => config.userSystem.getUserPower(id)
	});
}