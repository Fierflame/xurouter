"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.server = server;
exports.default = init;

var _response = require("./response");

var _response2 = _interopRequireDefault(_response);

var _util = require("./util.path-match");

var _util2 = _interopRequireDefault(_util);

var _map = require("./map");

var _link = require("./link");

var _router = require("./router");

var _info = require("./info");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//获取请求
/**
 * 获取请求信息
 * @param	{String}	path	路径
 * @param	{String}	cbName	路径
 * @param	{Object}	query	查询信息
 * @param	{Object}	body 	请求体
 * @return	{Object}			请求信息
 */
function requestInfo(cbName, path, query, body) {
	let data, cb;
	path = path.replace(/\.jsonp$/, x => (data = query, cb = data && data[cbName] || cbName, '')).replace(/[\/\\]+$/, "").split(/[\\\/]/);
	data = data && Object.keys(data).length && data || body && Object.keys(body).length && body || query || {};
	return { path, data, cb };
}

/**
 * 从列表中获取路由
 * @param		{Array}		path		路径
 * @param		{Function}	mapList		路径/路由列表生成器
 * @param		{Function}	getLink		获取虚拟链接
 * @param		{Function}	name2router	功能
 * @param		{Boolean}	ignore		是否忽略第一层
 */
function* route(path, mapList, getLink, getRouter, method, ignore) {
	//路径应为文件夹的数组
	if (Array.isArray(path)) {
		path = path.map(String);
	} else if (typeof path === "string") {
		path = path.split(/[\\\/]/);
	} else {
		return;
	}
	//循环找到第一个路径环境
	for (let [p, rf, appId] of (0, _util2.default)(path, mapList, getLink, ignore)) {
		//如果是名称，则需要通过名称获取路由函数
		if (typeof rf === "string" && getRouter) {
			rf = getRouter(rf, appId);
		}
		//如果路由不是函数，则跳过
		if (typeof rf !== "function") {
			continue;
		}
		//通过路由获取实际函数
		let f = rf(path.slice(p.length), method);
		if (!f) {
			return;
		}
		if (Symbol.iterator in f) {
			yield* f;
		} else if (typeof f === "function") {
			yield f;
		}
	}
}

/**
 * 动态处理
 * @param {String}		path					路径
 * @param {Object}		query					请求的query
 * @param {Object}		body					请求的body
 * @param {Object}		opt						userInfo的参数
 */
async function server(path, query, body, opt) {
	let cbName = this && this.cbName || "cb";
	let map = this && this.mapList || _map.list;
	let link = this && this.getLink || _link.get;
	let router = this && this.getRouter || _router.get;
	let getInfo = this && this.getInfo || _info.info;

	let data, cb, info;
	//获取真是路径、数据及回调函数名称
	({ path, data, cb } = requestInfo(cbName, path, query, body));
	try {
		for (let func of route(path, map, link, router, opt.method)) {
			if (!(func && typeof func === "function")) {
				continue;
			}
			info = info || (await getInfo(opt, query, body));
			const response = new _response2.default(cb);
			let ret;
			try {
				ret = await func(data, info, response);
			} catch (e) {
				response.error = e;
				return response;
			}
			if (ret instanceof _response2.default) {
				if (ret.valid) {
					return ret;
				}
			} else if (ret && response.valid) {
				response.value = ret;
				return response;
			}
		}
	} finally {
		if (opt.files) {
			for (let file of opt.files) {
				file.destroy();
			}
		}
	}
}

function init(config, mapList, getLink, getRouter, getInfo) {
	return server.bind({
		get cbName() {
			return config.cbName;
		},
		mapList, getLink, getRouter, getInfo
	});
}