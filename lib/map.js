"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.update = update;
exports.set = set;
exports.remove = remove;
exports.find = find;
exports.list = list;
exports.default = init;
let base = exports.base = []; // [[domain, ...vpath], path, appId]

function update(data) {
	if (!Array.isArray(data)) {
		return;
	}
	const maps = this || base;
	maps.splice(0);
	data.forEach(item => {
		if (!Array.isArray(item)) {
			return;
		}
		try {
			let [[domain, ...vpath], path, appId, main] = item;
			if (typeof path !== "string") {
				return;
			}
			if (typeof domain !== "string") {
				domain = "";
			}
			if (typeof appId !== "string") {
				appId = "";
			}
			if (typeof main !== "boolean") {
				main = false;
			}
			vpath = vpath.filter(v => v && typeof v === "string");
			maps.push([[domain, ...vpath], path, appId]);
		} catch (e) {
			return;
		}
	});
}

/**
 * 设置路由映射
 * @param {String} path   真实路径
 * @param {String} appId  所属应用Id
 * @param {String} vpath  映射路径
 * @param {String} domain 映射域名
 */
function set(path, appId, vpath, domain) {
	const maps = this || base;
	vpath = vpath.split("/");
	if (typeof appId !== "string") {
		appId = "";
	}
	if (vpath[0]) {
		vpath.unshift(domain);
	} else {
		vpath[0] = domain;
	}
	maps.push([vpath, path, appId]);
}

/**
 * 移除路径
 * @param {String} path   真实路径
 * @param {String} appId  所属应用Id
 * @param {String} vpath  映射路径
 * @param {String} domain 映射域名
 */
function remove(path, appId, vpath, domain) {
	const maps = this || base;
	if (typeof appId !== "string") {
		appId = "";
	}
	if (path === null) {
		for (let i = maps.length - 1; i >= 0; i--) {
			if (maps[i][2] === appId) {
				maps.splice(i, 1);
			}
		}
	} else {
		if (vpath.length && vpath[0] !== "/") {
			vpath = domain + "/" + vpath;
		} else {
			vpath = domain + vpath;
		}
		for (let i = maps.length - 1; i >= 0; i--) {
			let [tvpath, tpath, tappId] = maps[i];
			if (path === tpath && appId === tappId && tvpath.join("/") === vpath) {
				maps.splice(i, 1);
			}
		}
	}
}

/**
 * 根据Id搜索映射
 * @param {String}  path     真实路径
 * @param {String}  appId    所属应用Id
 * @param {Boolean} all      是否获取全部
 * @return {Array}			 找到的映射
 */
function find(path, appId, first) {
	const maps = this || base;
	if (typeof appId === "boolean") {
		[appId, first] = [first, appId];
	}
	if (typeof appId !== "string") {
		appId = typeof first === "string" ? "" : first;
	}
	try {
		if (first) {
			for (let [vpath, tpath, tappId, main] of maps) {
				if (path === tpath && appId === tappId) {
					const [domain, ...vp] = vpath;
					return ["/" + vp.join("/"), domain];
				}
			}
			return;
		}
		let ret = null;
		for (let [vpath, tpath, tappId, main] of maps) {
			if (path === tpath && appId === tappId) {
				if (first) {
					const [domain, ...vp] = vpath;
					return ["/" + vp.join("/"), domain];
				}
				if (main) {
					ret = vpath;
					break;
				}
				ret = ret || vpath;
			}
		}
		const [domain, ...vpath] = ret;
		return ["/" + vpath.join("/"), domain];
	} catch (e) {}
}

/**
 * 根据Id搜索映射
 * @param {String}  path     真实路径
 * @param {String}  appId    所属应用Id
 * @param {Boolean} all      是否获取全部
 * @return {Array}			 找到的映射
 */
function* list(path, appId) {
	const maps = this || base;
	if (path === undefined) {
		yield* maps;
	}
	if (path === true) {
		for (let [vpath, tpath, tappId, main] of maps) {
			const [domain, ...path] = vpath;
			yield ["/" + vpath.join("/"), domain];
		}
	}
	if (typeof appId !== "string") {
		appId = "";
	}
	for (let [vpath, tpath, tappId, main] of maps) {
		if (path === tpath && appId === tappId) {
			const [domain, ...path] = vpath;
			yield ["/" + vpath.join("/"), domain];
		}
	}
}

function init(maps = []) {
	return {
		set: set.bind(maps),
		remove: remove.bind(maps),
		find: find.bind(maps),
		list: list.bind(maps),
		update: update.bind(maps)
	};
}