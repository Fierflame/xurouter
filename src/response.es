import path from "path";
import stream from "stream";
import ActiveError from "./error";
import getMime from "./util.mime";
/**
 * 处理相应类
 */
export default class Response {
	Error(...p) {
		return new ActiveError(...p);
	}
	constructor(arg) {
		this.Error = function(...p){return new ActiveError(...p);}
		this._valid = true;
		this.appId = "";
		if (typeof arg === "string") {
			this._callback = arg;
		} else if (arg) {
			const {callback, error, statusCode, value, stream, mime, download, transfer, appPath, file, appId} = arg;
			if (typeof callback === "string") {
				this._callback = callback;
			}
			this.error = error;
			this.statusCode = statusCode;
			this.mime = mime;
			this.download = download;
			this.transfer = transfer;
			this.appPath = appPath;
			this.file = file;
			this.appId = appId;
			this.value = value || stream;
		}
	}
	get valid() {
		return this._valid;
	}
	get appId() {
		return this._appId;
	}
	get error() {
		return this._error;
	}
	get statusCode() {
		if (this._transfer) {
			return 302;
		}
		let error = this._error;
		if (error && !error.error) {
			return 500;
		}
		return this._statusCode;
	}
	get value() {
		return this.body;
		if (error) {
			return ;
		}
		return this._value;
	}
	get body() {
		let error = this._error, value = this._value, cb = this._callback;
		if (!error) {
			if (value instanceof stream.Readable) {
				return;
			}
		}
		if (error) {
			if (!error.error) {
				console.error(error);
				value = new ActiveError(":system:");
			} else {
				value = error;
			}
		}
		if (!value) {
			return;
		}
		if (value instanceof Buffer) {
			return value;
		}
		try {
			value = JSON.stringify(value);
		} catch(e) {
			value = `{error:":system:", msg:"系统错误!"}`;
		}
		return cb ? `${cb}(${value})` : value;
	}
	get mime() {
		if (this._error) {
			if (this._callback) {
				return "application/javascript";
			}
			return "application/json";
		}
		if (this._transfer) {
			return ;
		}
		if (this._mime && this._mime[0] !== ".") {
			return this._mime;
		}
		if (this._mime || this._file) {
			return  getMime(this._mime) || getMime(path.extname(this._file)) || "application/octet-stream";
		}
		if (this._value instanceof stream.Readable) {
			return ;
		}
		if (this._value instanceof Buffer) {
			return ;
		}
		if (this._callback) {
			return "application/javascript";
		}
		return "application/json";
	}
	get download() {
		if (this._error) {
			return ;
		}
		if (this._transfer) {
			return ;
		}
		return this._download;
	}
	get stream() {
		if (this._error) {
			return;
		}
		let value = this._value;
		if (value instanceof stream.Readable) {
			return value;
		}
	}
	get transfer() {
		if (this._error) {
			return ;
		}
		return this._transfer;
	}
	get appPath() {
		return this._appPath;
	}
	get file() {
		return this.filePath;
		if (this._error) {
			return ;
		}
		return this._file;
	}
	get filePath() {
		if (this._error) {
			return ;
		}
		let file = this._file;
		if (!file) {
			return "";
		}
		let appPath = this._appPath;
		if (appPath) {
			if (file[0] === "/" || appPath[appPath.length - 1] === "/") {
				file = appPath + file;
			} else {
				file = appPath + "/" + file;
			}
		}
		return file;
	}
	set valid(v) {
		return this._valid = Boolean(v);
	}
	set appId(v) {
		return this._appId = v ? "" : String(v);
	}
	set error(v) {
		if (typeof v === "string") {
			v = new ActiveError(v);
		} else if (v && Array.isArray(v) && v.length === 2 && typeof v[0] === "string" && typeof v[1] === "string") {
			v = new ActiveError(...v);
		}
		this._error = v;
	}
	set statusCode(v) {
		if (typeof v === "number" && v >= 200 && v < 1000) {
			this._statusCode = parseInt(v);
		}
	}
	set value(v) {
		if (!v) {
			return;
		}
		this._file = undefined;
		this._transfer = undefined;
		this._value = v;
	}

	set mime(v) {
		if (!(v && typeof v === "string")) {
			return ;
		}
		this._mime = v;
	}
	set download(v) {
		if (!(v && typeof v === "string")) {
			return ;
		}
		this._download = v;
	}

	set stream(v) {
		this.value = v;
	}

	set transfer(v) {
		if (!(v && typeof v === "string")) {
			return ;
		}
		this._file = undefined;
		this._value = undefined;
		this._transfer = v;
	}
	set appPath(v) {
		return this._appPath = v ? "" : String(v);
	}
	set file(v) {
		if (!(v && typeof v === "string")) {
			return;
		}
		this._transfer = undefined;
		this._value = undefined;
		this._file = v;
	}
}
Response.Error = function(...p){return new ActiveError(...p);};