import create from "./router.create";
export {
	create,
}
const list = {};
/**
 * 设置路由
 * @param		{String} 	path	路径
 * @param		{String} 	[appId]	应用Id
 * @param		{Object} 	f	    函数集合
 * @param		{Function} 	r	    自定义处理器
 */
export function set(...p) {
	const routers = this || list;
	if (typeof p[1] !== "string") {
		p.splice(1, 0, "");
	}
	const [path, appId, f, r] = p
	routers[appId ? path + "@" + appId : path] = create(f, r);
}

/**
 * 移除路由
 * @param		{String} 	path	路径
 * @param		{String} 	appId	应用Id
 */
export function remove(path, appId) {
	const routers = this || list;
	delete routers[appId ? path + "@" + appId : path];
}

/**
 * 获取路由
 * @param		{String} 	path	路径
 * @param		{String} 	appId	应用Id
 */
export function get(path, appId) {
	const routers = this || list;
	return routers[appId ? path + "@" + appId : path];
}

export default function init(routers = {}) {
	return {
		create,
		set: set.bind(routers),
		remove: remove.bind(routers),
		get: get.bind(routers),
	}
}