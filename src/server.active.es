import Response from "./response";
import pathMatch from "./util.path-match";

import {list as mapList} from "./map";
import {get as getLink} from "./link";
import {get as getRouter} from "./router";
import {info as createInfo} from "./info";

//获取请求
/**
 * 获取请求信息
 * @param	{String}	path	路径
 * @param	{String}	cbName	路径
 * @param	{Object}	query	查询信息
 * @param	{Object}	body 	请求体
 * @return	{Object}			请求信息
 */
function requestInfo(cbName, path, query, body) {
	let data, cb;
	path = path
		.replace(/\.jsonp$/,x => (data = query, cb = data && data[cbName] || cbName, ''))
		.replace(/[\/\\]+$/,"")
		.split(/[\\\/]/);
	data = data && Object.keys(data).length && data || body && Object.keys(body).length && body || query || {};
	return {path, data, cb};
}

/**
 * 解析获取路由信息
 * @param	{Array<String>}		path			路径数组
 * @param	{String | Function}	rf				模块名称或路由函数
 * @param	{String}			appId			应用ID
 * @param	{String}			method			采用的方法
 * @this	{Function}			getRouter		路由函数转换函数
 * @yield	{Function}							处理函数
 */
export function*parseRoute(path, rf, appId, method, getRouter) {
	//如果是名称，则需要通过名称获取路由函数
	if (typeof rf === "string" && getRouter) {
		rf = getRouter(rf, appId);
	}
	//如果路由不是函数，则跳过
	if (typeof rf !== "function") {
		continue;
	}
	//通过路由获取实际函数
	let f = rf(path.slice(p.length), method);
	if (!f) {
		continue ;
	}
	if (Symbol.iterator in f) {
		if (typeof f === "function") {
			yield*f;
		}
	} else if (typeof f === "function") {
		yield f;
	}
}

/**
 * 从列表中获取路由
 * @param		{Array}		path		路径
 * @param		{Function}	mapList		路径/路由列表生成器
 * @param		{Function}	getLink		获取虚拟链接
 * @param		{Function}	getRouter	功能
 * @param		{Boolean}	ignore		是否忽略第一层
 * @yield		{Function}				处理函数
 */
function*route(path, mapList, getLink, getRouter, method, ignore) {
	//路径应为文件夹的数组
	if (Array.isArray(path)) {
		path = path.map(String);
	} else if (typeof path === "string"){
		path = path.split(/[\\\/]/);
	} else {
		return ;
	}
	//循环找到第一个路径环境
	for(let [p, rf, appId] of pathMatch(path, mapList, getLink, ignore)) {
		yield*parseRoute(path.slice(p.length), rf, appId, method, getRouter);
	}
}

export async function exec(func, data, info, response) {
	let ret;
	try {
		ret = await func(data, info, response);
	} catch(e) {
		response.error = e;
		return response;
	}
	if (ret instanceof Response) {
		if (ret.valid) {
			return ret;
		}
	} else if (ret && response.valid) {
		response.value = ret;
		return response;
	}
}

/**
 * 动态处理
 * @param {String}		path					路径
 * @param {Object}		query					请求的query
 * @param {Object}		body					请求的body
 * @param {Object}		opt						userInfo的参数
 */
export async function server(path, query, body, opt) {
	let cbName = this && this.cbName || "cb";
	let map = this && this.mapList || mapList;
	let link = this && this.getLink || getLink;
	let router = this && this.getRouter || getRouter;
	let getInfo = this && this.getInfo || createInfo;

	let data, cb, info;
	//获取真是路径、数据及回调函数名称
	({path, data, cb} = requestInfo(cbName, path, query, body));
	try {
		for (let func of route(path, map, link, router, opt.method)) {
			if (!(func && typeof func === "function")) {
				continue;
			}
			info = info || await getInfo(opt, query, body);
			const response = await exec(func, data, info, new Response(cb));
			if (response) {
				return response;
			}
		}
	} finally {
		if (opt.files) {
			for (let file of opt.files) {
				file.destroy();
			}
		}
	}
}

export default function init(config, mapList, getLink, getRouter, getInfo) {
	return server.bind({
		get cbName() {
			return config.cbName;
		},
		mapList, getLink, getRouter, getInfo,
	});
}