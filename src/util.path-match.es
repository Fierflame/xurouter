
/**
 * 路径匹配
 * @param {Array}			p			相对路径
 * @param {Array}			brn			虚拟路径
 * @param {String}			appId		应用id
 * @param {Function}		getLink		获取路径链接函数
 * @param {Array}			basePath	基本路径
 * @yield {PathDescribe}				路径描述
 */
function*pathRecursion(p, brn, appId, getLink, basePath = []) {
	//是否未输出原始路基
	let base = true;
	brn = Array.isArray(brn) ? brn.filter(x=>x && typeof x === "string") : brn.split(/\\\//).filter(x=>x);
	for(let path = [...brn, ...p], length = brn.length; length >= 0; length--) {
		//获取路径链接
		const pl = getLink(length ? path.slice(0, length).join("/") : "", appId);
		if (pl) {
			//获取对应路径
			for (let info of pl(path.slice(length))) {
				if (info) {
					const [p, rp, appId] = info;
					yield*pathRecursion(path.slice(length + p.length),  rp, appId, getLink, [...basePath, ...p]);
				} else if (base) {
					base = false;
					yield [basePath, brn.join("/"), appId];
				}
			}
		}
	}
	if (base) {
		yield [basePath, brn.join("/"), appId];
	}
}
/**
 * 路径匹配
 * @param {Array}			path	原始路径数组
 * @param {Array}			getList	映射列表
 * @param {Function}		getLink	
 * @param {Boolean}			ignore	是否忽略根路径
 * @yield {PathDescribe}			路径描述
 */
export default function*pathMatch(path, getList, getLink, ignore) {
	path = Array.from(path);
	if(!path.length) {
		return ;
	}
	if (!path[path.length - 1]) {
		path.pop();
	}
	for(let [p, rp, appId] of getList()) {
		//路径长度必须符合要求
		if (path.length < p.length) {
			continue;
		}
		//逐层路径相等
		for (
			let i = (path[0] && p[0] && !ignore) ? 0 : 1,l = p.length;
			i < l || (yield*pathRecursion(path.slice(p.length), rp, appId, getLink, p)) && false;
			i++
		) {
			if (path[i] !== p[i]) {
				break;
			}
		}
	}
}