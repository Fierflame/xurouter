import path from "path";
import {find as findMap} from "./map";

/**
 * 根据功能路径获取Url
 * @param  {String} pathId 功能路径
 * @return {String}        实际Url
 */
export function get(pathId) {
	let [id, ep = ""] = String(pathId).split(":");
	id = id.split("@");
	try {
		const [basePath, domain] = (this && this.findMap || findMap)(id[0], id[1]);
		const port = this && this.port || "";
		const protocol = this && this.protocol || "";
		return (domain && (protocol + "//" + domain + (port && ":" + port))) + path.join("/", basePath, path.join("/", ep));
	} catch (e) {
		return "";
	}
}
export default function init(findMap, config) {
	return {
		get: get.bind({
			findMap,
			get port(){return config.port},
			get protocol(){return config.protocol},
		}),
	};
}