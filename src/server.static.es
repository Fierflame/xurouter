import fs from "fs";
import Response from "./response";
import pathMatch from "./util.path-match";

import {list as mapList} from "./map";
import {get as getLink} from "./link";

//基本文档路径和应用路径
let htdocsPath, appPath;
const stat = path => new Promise((a,b)=>fs.stat(path, (e, d) => e?b(e):a(d) ));
/**
 * 获取真实路径
 * @param  {String} p 转化后路径
 * @return {Array}   真实路径信息
 */
async function getRealPath(appPath, realPath, rpath, vpath, appId) {
	//如果为目录，则返回index.html
	if (realPath[realPath.length - 1] === "/") {
		return new Response({file: realPath + "index.html", appId, appPath});
	}
	//如果文件存在,且为文件，则返回
	let isDir;
	try {
		let info = await stat(appPath + realPath);
		if (info.isFile()) {
			return new Response({file: realPath, appId, appPath});
		}
		isDir = info.isDirectory();
	} catch(e) {}
	//如果文件.html存在，且为文件，则返回
	try {
		if ((await stat(appPath + realPath + ".html")).isFile()) {
			return new Response({file: realPath + ".html", appId, appPath});
		}
	} catch(e) {}
	//如果文件夹存在，则302
	if (isDir) {
		return new Response({transfer: `${vpath}${rpath}/`, appId, appPath});
	}
}
function getBasePath(appId) {
	return appId ? `${appPath}/${appId}/htdocs/` : `${htdocsPath}/`;
}
/**
 * 静态页面
 * @param {String}	staticPath	路径
 */
export async function server(staticPath) {
	//路径应为文件夹的数组
	let map = this && this.mapList || mapList;
	let link = this && this.getLink || getLink;
	let basePath = this && this.basePath || getBasePath;

	staticPath = Array.isArray(staticPath) ?
		staticPath.map(String) :
		(typeof staticPath === "string" ? staticPath.split(/[\\\/]+/) : []);

	//循环找到第一个路径环境
	for(let [vpath, path, appId] of pathMatch(staticPath, map, link)) {
		if (typeof path === "string") {
			let rpath = staticPath.slice(vpath.length);
			rpath = rpath.length ? "/" + rpath.join("/") : "";
			//获取实际路径
			const res = await getRealPath(
				basePath(appId),
				path + rpath,
				rpath,
				vpath.length > 1 ? "/" + vpath.slice(1).join("/") : "",
				appId
			);
			if (res) {
				return res;
			}
		}
	}
}

export default function init(config, mapList, getLink, getRouter) {
	return server.bind({
		basePath: appId => config.getBasePath(appId),
		mapList, getLink, getRouter,
	});
}
