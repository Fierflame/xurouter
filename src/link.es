//虚拟路径链接生成器列表，形如：
// `${path}@${appId}`: *(path) => [[...vpath], path, appId]
const list = {};

export function set(link, path, appId) {
	if (typeof link !== "function") {
		return;
	}
	const links = this || list;
	links[appId ? path + "@" + appId : path] = link;
}
export function remove(path, appId) {
	const links = this || list;
	delete links[appId ? path + "@" + appId : path];
}

export function get(path, appId) {
	path = appId ? `${path}@${appId}` : path;
	const links = this || list;
	if (path in links) {
		return links[path];
	}
}

export default function init(links = {}) {
	return {
		set: set.bind(links),
		remove: remove.bind(links),
		get: get.bind(links),
	}
}
